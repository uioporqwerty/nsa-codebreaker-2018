Task 0 - Warm Up - (Network Traffic Analysis)

We have acquired a packet capture (PCAP) file showing network traffic between a newly infected victim computer and the attacker's listening post (LP). The LP is simply a server that is listening for incoming connections on a certain port. To get started, analyze the network capture and submit the IP address of the attacker's LP.

Solution:
172.28.57.176 is the attacker IP address because the port number 9999 is a common cryptocurrency port number.